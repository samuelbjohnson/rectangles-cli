var readline = require('readline')
var EventEmitter = require('events')
var _ = require('underscore')
var Rectangle = require('rectangles/rectangles')

var prompter = readline.createInterface(process.stdin, process.stdout)

var events = new EventEmitter()
var log = console.log

var buildRectangleAndThen = function(greeting, callback) {
	console.log(greeting)
	console.log("Define a corner")
	
	var inputs = {}
	
	function getX() {
		prompter.question("X: ", function(x) {
			var parsedX = parseFloat(x)
			if (_.isNaN(parsedX)) {
				log("X must be a number")
				getX()
			} else {
				inputs.x = parsedX
				getY()
			}
		})
	}
	
	function getY() {
		prompter.question("Y: ", function(y) {
			var parsedY = parseFloat(y)
			if (_.isNaN(parsedY)) {
				log("Y must be a number")
				getY()
			} else {
				inputs.y = parsedY
				getWidth()
			} 
		})
	}
	
	function getWidth() {
		prompter.question("Rectangle Width: ", function(width) {
			var parsed = parseFloat(width)
			if (_.isNaN(parsed)) {
				log("Width must be a number")
				getWidth()
			} else {
				if (parsed == 0) {
					log("Width must be non-zero")
					getWidth()
				} else {
					inputs.width = parsed
					getHeight()
				}
			}
		})
	}
	
	function getHeight() {
		prompter.question("Rectangle Height: ", function(height) {
			var parsed = parseFloat(height)
			if (_.isNaN(parsed)) {
				log("Height must be a number")
				getHeight()
			} else {
				if (parsed == 0) {
					log("Height must be non-zero")
					getHeight()
				} else {
					inputs.height = parsed
					var rect = new Rectangle(inputs.x, inputs.y, inputs.width, inputs.height)
					callback(rect)
				}
			}
		})
	}
	
	getX()
}

events.on('stop', function() {
	prompter.close()
	process.exit()
})

events.on('analyze', function(r1, r2) {
	var intersections = r1.findIntersections(r2)
	if (intersections.length > 0) {
		var intersectionReport = "Your rectangles intersect at "
		intersectionReport += _.map(intersections, function(point) {
			return "(" + point.x + ", " + point.y + ")"
		}).join()
		log(intersectionReport)
	} 
	
	if (r1.isAdjacentTo(r2)) {
		log("Rectangles are adjacent")
	}
	
	if (r1.isContainedWithin(r2)) {
		log("Your second rectangle contains your first")
	}
	if (r2.isContainedWithin(r1)) {
		log("Your first rectangle contains your second")
	}
	
	log("Analysis Complete")
	
	
	prompter.question('Do you want to analyze more rectangles (y or n)? ', function(response) {
		if (response.toLowerCase() === 'yes' || response.toLowerCase() === 'y') {
			events.emit('start')
		} else {
			events.emit('stop')
		}
	})
})

events.on('start', function() {
	buildRectangleAndThen("Tell me about a rectangle", function(rect1) {
		buildRectangleAndThen("Tell me about another rectangle", function(rect2) {
			events.emit('analyze', rect1, rect2)
		})
	})
})

events.emit('start')