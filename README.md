# README

## Rectangles CLI

A command-line interface for the Rectangles module

## Getting Started

Rectangles assumes you have a working installation of nodejs and npm. You can install Rectangles by cloning this repository. Then, from the command line, navigate to the base directory and run `npm install` to download the necessary dependencies. You should then be able to run the basic command line interface by running `node main` from the command line.

### Using the basic UI

The command line interface will ask you to define two rectangles, after which it will analyze them for intersection, adjacency, and containment. If none of those are found, you'll simply see the `analysis complete` message. After an analysis is complete, you'll be asked whether you want to start over.